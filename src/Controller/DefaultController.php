<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
	/**
	 * @Route("/", name="homepage")
	 */
	public function index()
	{
//		$Importer = new \App\Importer('https://scores.frisbee.pl/scores/mobile');
//		$Importer->importPlayers();

		return $this->render('base.html.twig', [
		]);
	}

	/**
	 * @Route("/signup", name="signup")
	 */
	public function signUp()
	{
//		$Importer = new \App\Importer('https://scores.frisbee.pl/scores/mobile');
//		$Importer->importPlayers();

		return $this->render('signUp.html.twig', [
		]);
	}
}
