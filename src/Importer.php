<?php

namespace App;


use Monolog\Handler\StreamHandler;
use Symfony\Bridge\Monolog\Logger;

class Importer
{
	/** @var string */
	private $tournamenrUrl;

	public function __construct(string $tournamenrUrl)
	{
		$this->tournamenrUrl = $tournamenrUrl;
	}

	public function importPlayers()
	{
		$start = microtime(true);

		$htmlContent = file_get_contents($this->tournamenrUrl . '/?view=stats');

		$DOM = new \DOMDocument();
		libxml_use_internal_errors(true);
		$DOM->loadHTML('<?xml encoding="utf-8" ?>' . $htmlContent);
		$a = $DOM->getElementsByTagName('a');
		$playersUrls = [];
		foreach ($a as $element) {
			if ($element->textContent === 'Pokaż wszystkich...') {
				$playersUrls[] = $this->tournamenrUrl . $element->getAttribute('href');
			}
		}

		$players = [];
		foreach ($playersUrls as $url) {
			$htmlContent = file_get_contents($url);
			$DOM->loadHTML('<?xml encoding="utf-8" ?>' . $htmlContent);
			$rows = $DOM->getElementsByTagName('tr');
			foreach ($rows as $row) {
				$playerInfo = $row->getElementsByTagName('td');
				if ($playerInfo[0]) {
					$players[] = [
						'playerInfo' => $this->getPlayerInformation($playerInfo[1]),
						'matches' => $playerInfo[2]->textContent,
						'assists' => $playerInfo[3]->textContent,
						'points' => $playerInfo[4]->textContent,
					];
				}
			}
		}
		$end = microtime(true);
		echo "The code took " . ($end - $start) . " seconds to complete.";
		var_dump($players);
		die;
	}

	private function getPlayerInformation(\DOMElement $playerCell): array
	{
		$a = $playerCell->getElementsByTagName('a');
		$span = $playerCell->getElementsByTagName('span');
		$profileUrl = $a[0]->getAttribute('href');

		preg_match('/[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ-]+\s[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ-]+/', $a[0]->textContent, $name);
		preg_match('/(\d)+/', $a[0]->textContent, $number);
		$log = new Logger('name');
		$log->pushHandler(new StreamHandler('your.log', Logger::WARNING));
		$log->warning($name[0]);

		return [
			'name' => $name[0],
			'number' => $number[0],
			'photo' => $this->getPlayerPhoto($profileUrl),
			'team' => $span[0]->textContent,
		];
	}

	private function getPlayerPhoto(string $url): string
	{
		$DOM = new \DOMDocument();
		$htmlContent = file_get_contents($this->tournamenrUrl . '/' . $url);
		$DOM->loadHTML('<?xml encoding="utf-8" ?>' . $htmlContent);
		$images = $DOM->getElementsByTagName('img');

		if ($images->count() > 1) {
			return 'https://scores.frisbee.pl/' . $images[1]->getAttribute('src');
		}

		return '';
	}
}
