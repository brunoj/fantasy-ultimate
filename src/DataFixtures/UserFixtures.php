<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends BaseFixture
{
	protected function loadData(ObjectManager $manager)
	{
		$this->createMany(10, 'main_users', function ($i){
			$User = new User();
			$User->setEmail(sprintf('spacebar%d@example.com', $i));
			$User->setPassword(sprintf('password%d', $i));

			return $User;
		});

		$manager->flush();
	}
}
